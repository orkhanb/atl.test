package az.atl.academy.service.impl;

import az.atl.academy.dao.repo.StudentRepository;
import az.atl.academy.model.dto.StudentDto;
import az.atl.academy.service.StudentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class StudentServiceImpl implements StudentService {

    private final MessageSource messageSource;
    private final StudentRepository studentRepository;
    private static List<StudentDto> studentDtoList = new ArrayList<>();
    private static Long counter = 0L;

    {
        var s1 = StudentDto.builder()
                .id(counter++)
                .fullName("Emil Aliyev")
                .phoneNumber("1234567")
                .age(20).build();
        var s2 = StudentDto.builder()
                .id(counter++)
                .fullName("Namiq  Babaye")
                .phoneNumber("1233333")
                .age(22).build();
        studentDtoList.addAll(List.of(s1, s2));
    }

    @Override
    public void create(StudentDto studentDto) {
//        Locale locale = LocaleContextHolder.getLocale();
//        log.info("Locale: " + locale);
//        Object[] objs = new Object[2];
//        objs[0] = studentDto.getFullName();
//        objs[1] = studentDto.getStudentId();
//        String message = messageSource.getMessage(GREETINGS.getMessage(), objs, locale);
//        log.info("Message: " + message);
//
//        var studentEntity = STUDENT_BUILDER.buildEntity(studentDto);
//        studentRepository.save(studentEntity);
//
//        log.info("Student created: " + studentDto);
        studentDto.setId(counter++);
        studentDtoList.add(studentDto);
    }

    @Override
    public StudentDto getById(Long id) {
//        Locale locale = LocaleContextHolder.getLocale();
//        var entity = studentRepository.findById(id);
//        if (entity.isPresent())
//            return STUDENT_BUILDER.buildDto(entity.get());
//
//        Object[] objs2 = new Object[1];
//        objs2[0] = id;
//        throw new RuntimeException(messageSource.getMessage(USER_NOT_FOUND.getMessage(), objs2, locale));
        var student = studentDtoList.stream().filter(data -> data.getId() == id).findFirst();
        if (student.isPresent())
            return student.get();
        throw new RuntimeException("Student is not found!");
    }

    @Override
    public List<StudentDto> getAll() {
        return studentDtoList;
    }

    @Override
    public void deleteById(Long id) {
        studentDtoList.removeIf(data -> data.getId() == id);
    }
}
